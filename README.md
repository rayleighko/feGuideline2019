# feGuideline2019  🧠

지름길보다는 정석으로 성장하려는 어느 Front-end Engineer의 학습 기록입니다. 일이 바쁘지 않을 때 자기 개발 혹은 정보 공유 목적으로 작성됩니다.

## 들어가기 전에 ✋

이 레포지토리는 지극히 주관적으로 Junior FE Engineer가 알면 좋을 내용에 대해 작성되고 있습니다. 만약 시간이 되신다면 본문을 읽으시기 전에 [서문](INTRODUCTION.md)을 보신다면 레포지토리의 내용을 읽거나 기여하는 데 도움이 될 것입니다. 더불어 주관적인 생각이 가미되기 때문에 다소 불편한 부분이 있을 수 있으니 그 점에 유의하시기 바랍니다.

> 더불어 Web FE가 아닌 다른 분야의 가이드를 작성하고 싶으시다면 'fork_me' 브랜치를 참고하세요.

## 방식 📋

기본적으로 각 주제 별로 꼭 알아야(외워야) 할 내용에 대해서 기술해야 합니다. 더불어 이 곳에 작성되는 모든 내용은 추상적이지 않고 현실적으로 도움이 될 거라고 생각하는 내용이어야 합니다. 또한, 개인적인 생각이 아닌 보편적으로 당연시되고 있는 내용이라면 해당 주제와 관련된 추천 문서나 도서를 함께 적고, 참고자료가 있다면 반드시 본문 하단에 기술해야 합니다.

> 본문에 작성된 내용 외에 추가적으로 넣고 싶은 내용이 있으시면 먼저 [ETC](/ETC)에 적어주시면 됩니다.
>
> 더불어 전체적인 문맥의 어투는 '~했다', '~이다'와 같이 짧게 끊어주시면 됩니다.

우선 목차는 트리 형식을 기반으로 최상단에는 전체 주제를 기입하고 그 하위의 세부 주제를 다시 그 하위 디렉터리로 옮기거나 해당 디렉터리의 `README.md`에 작성합니다. 그리고 읽는 이의 편의를 고려해 내가 있는 디렉터리 바로 하위의 내용에 대해서는 링크를 거는 것을 원칙으로 합니다. 그럼 모두 화이팅!

## 소개 📓

무엇이 '잘' 하는 개발자인가요? 이 지극히 주관적일 수밖에 없는, 혹은 어느정도 그 기본 틀은 잡혀있다고 생각하는 그 주제에 대한 고민과 그 답으로 작성되는 주관적인 레포지토리입니다. 그래서 본격적으로 글을 읽으시기 전에 최초 작성자가 바라 본 개발은 비즈니스를 풀어가는 하나의 방법일 뿐이라는 것을 명심해주세요. 더불어 '개발'이라는 것을 하나의 '일'로 보고 있기 때문에, '일' 잘하는 사람이 되기 위한 고민의 흔적을 작성했다고 생각해주시면 좋을 것 같습니다. :)

> 따라서 취미로 개발하시는 분들은 목차에서 제시하는 내용이 다소 필요없게 느껴지실 수도 있습니다.

그래서 최초 작성자는 앞서 말한 '잘'이라는 초점을 '개발'이나 '구현', '알고리즘', '설계'와 같이 특정한 주제가 아닌 '일'에 맞춰 서술할 것입니다. 이 레포지토리는 그런 취지로 만들어졌기 때문입니다.

### 목차 💻

- [Common](COMMON/)

  - Troubleshooting(문제 추적)
  - Design(설계)
  - Implementation(구현) - 작성중
  - Configuration Management(형상 관리) with Git - 작성중
  - Efficient Development(효율적인 개발) - 작성중
  - Object-oriented Programming(객체 지향 프로그래밍) - 작성중
  - Function-oriented Programming(함수 지향 프로그래밍) - 작성중
  - Test-driven Development(테스트 주도 개발) - 작성중
  - Domain-driven Development(도메인 주도 개발) - 작성중
  - Application Programming Interface(API, 응용 프로그램 프로그래밍 인터페이스) - 작성중

- [PS(Problem Solving)](PS/)

  - Coding Test Tips - 작성중
  - How to study Problem Solving?

- [CS(Computer Science)](CS/)

  - Data Structure - 작성중
  - Network - 작성중
  - Operating System - 작성중
  - Database - 작성중
  - Design Pattern - 작성중

- [SOFT SKILL](SOFTSKILL/)

  - 글쓰기 - 작성중
  - 말하기 - 작성중
  - 동료를 위한 개발 - 작성중

- [WEB](WEB/)

  - HTML - 작성중
  - CSS - 작성중
  - Javascript - 작성중
  - Advanced Typescript - 작성중
  - Web UI Tools - 작성중
  - Advanced React - 작성중
  - Web Browser - 작성중
  - Advanced Chrome Dev tools - 작성중

- [ETC](ETC/)

  - English - 작성중
  - Web related Open source - 작성중
  - Cloud for FE - 작성중
  - Docker - 작성중
  - NginX - 작성중
  - front-end-handbook-2019
